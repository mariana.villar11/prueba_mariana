suppressPackageStartupMessages(require(RODBC))
suppressPackageStartupMessages(require(data.table))

checkDataConsistency <- function(mysql_connection, table_name, columns_names, newdata) {
  
  # New (column, value) pairs
  newdata_values <- newdata[, lapply(.SD, as.character), .SDcols=columns_names]
  newdata_values <- unique(melt(newdata_values, measure.vars=columns_names, variable.name="col_name", value.name="value"))
  newdata_values <- newdata_values[!is.na(value)]
  newdata_values <- paste0("(\"", table_name, "\", \"", newdata_values$col_name, "\", \"", 
                           gsub("\"", "\\\\\"", newdata_values$value), "\")") # Escapamos " en valores para escribir la query
  
  # Old (column, value) pairs
  query <- paste0("SELECT ColumnName, Value FROM Datos._CategoryValues",
                  " WHERE TableName = \"", table_name, "\"",
                  " AND (", paste(paste0("ColumnName = \"", columns_names, "\""), collapse=" OR "), ");")
  oldata_values <- sqlQuery(mysql_connection, query)
  oldata_values <- paste0("(\"", table_name, "\", \"", oldata_values$ColumnName, "\", \"", 
                          gsub("\"", "\\\\\"", oldata_values$Value), "\")") # Escapamos " en valores para escribir la query
  
  # Insert new pairs and throw warning (if any)
  changes <- setdiff(newdata_values, oldata_values)
  if (length(changes) > 0) {
    insert_query <- paste0("INSERT INTO Datos._CategoryValues VALUES ",
                           paste(changes, collapse=", "), ";")
    sqlQuery(mysql_connection, insert_query)
    
    warning(paste("New categorical values found:", paste(changes, collapse=", ")))
  }
}


# # EJEMPLOS
# 
# table_name <- "CarActivities"
# 
# 
# columns_names <- c("EmailDomain", "Marca", "Modelo", "Combustible", "Potencia", "Version",
#                    "TipoVehiculo", "PropietarioCoche", "AcabaDeComprarlo", "ComoEsCocheAComprar",
#                    "UsoDelCoche","TieneParking","KmAnuales","Remolque","DriveAnotherCar","CarInFamily",
#                    "PaisConductorPincipal", "PaisCarneConductorPrincipal", "Genero", "EstadoCivil", "HijosMenores16",
#                    "Ocupacion", "Provincia", "ComAutonoma", "TiempoMismaDireccion", 
#                    "QuienEsTomador", "GeneroTomador", "EstadoCivilTomador",
#                    "HayConductorOcasional", "ConductorOcasionalMasJoven", "GeneroOcasional", "EstadoCivilOcasional",
#                    "OcupacionOcasional","ConduceOtroCocheOcasional",
#                    "AseguradoraAnterior", "Multas", "RiskCoverType", "PolicyHolderCompany", 
#                    "DeviceOS", "DeviceType", "UtmSource", "UtmCampaign", "UtmTerm")