#Instalo librer�as y cargo las librer�as necesarias
install.packages("data.table")
install.packages("rpart")
install.packages("rpart.plot")
install.packages("ROCR")
install.packages("e1071")
install.packages("MASS")
install.packages("class")
install.packages("pROC")
install.packages("caret")
suppressWarnings(suppressMessages(library(data.table)))
suppressWarnings(suppressMessages(library(rpart)))
suppressWarnings(suppressMessages(library(rpart.plot)))
suppressWarnings(suppressMessages(library(ROCR)))
suppressWarnings(suppressMessages(library(e1071)))
suppressWarnings(suppressMessages(library(MASS)))
suppressWarnings(suppressMessages(library(class)))
suppressWarnings(suppressMessages(library(pROC)))
suppressMessages(library(caret))

#Selecciono el directorio de trabajo
setwd("C:/Users/mariana.villar/Desktop/CURSO R/BEEVA")

#Los dos datasets
#Curso de portugu�s
student_por <- fread("student-por.csv", stringsAsFactors = FALSE, showProgress = TRUE, data.table = TRUE)
#Curso de matem�ticas
student_mat <- fread("student-mat.csv", stringsAsFactors = FALSE, showProgress = TRUE, data.table = TRUE)
#Genero una variable extra binaria que es Asignatura (por/mat)
student_por <- student_por[,Asignatura := rep("por",nrow(student_por))]
student_mat <- student_mat[,Asignatura := rep("mat",nrow(student_mat))]
#Uno los data tables
estudiantes <- rbind(student_mat,student_por)
#Transformo las 3 variables de notas a enteros
estudiantes$G1 <- as.integer(estudiantes$G1)
estudiantes$G2 <- as.integer(estudiantes$G2)
estudiantes$G3 <- as.integer(estudiantes$G3)

#########################################################
## Transformaci�n de Variables ##
# Cargar el archivo functions.R 

#Cargo las funciones de generaci�n de variables (Es necesario cambiar la ruta)
source("C:/Users/mariana.villar/Desktop/CURSO R/BEEVA/functions.R")

#########################################################

#Formo el datatable con las variables ya transformadas
DTEstudiantes <- data.table(EsColegioGP = genEsColegioGP(estudiantes),
                            EsHombre = genEsHombre(estudiantes),
                            Edad = estudiantes$age,
                            EsUrbano = genEsUrbano(estudiantes),
                            EsNucleoFamMayIgual3 = genEsNucleoFamMayIgual3(estudiantes),
                            EstanPadresJuntos = genEstanPadresJuntos(estudiantes),
                            MadreEducacion = estudiantes$Medu,
                            PadreEducacion = estudiantes$Fedu,
                            MadreTrabajaEnCasa = genMadreTrabajaEnCasa(estudiantes),
                            MadreTrabajaEnSalud = genMadreTrabajaEnSalud(estudiantes),
                            MadreTrabajaEnServicios = genMadreTrabajaEnServicios(estudiantes),
                            MadreTrabajaDeProfesora = genMadreTrabajaDeProfesora(estudiantes),
                            PadreTrabajaEnCasa = genPadreTrabajaEnCasa(estudiantes),
                            PadreTrabajaEnSalud = genPadreTrabajaEnSalud(estudiantes),
                            PadreTrabajaEnServicios = genPadreTrabajaEnServicios(estudiantes),
                            PadreTrabajaDeProfesora = genPadreTrabajaDeProfesora(estudiantes),
                            RazonParaElegirCentroCursos = genRazonParaElegirCentroCursos(estudiantes),
                            RazonParaElegirCentroCercania = genRazonParaElegirCentroCercania(estudiantes),
                            RazonParaElegirCentroReputacion = genRazonParaElegirCentroReputacion(estudiantes),
                            EsTutorMadre = genEsTutorMadre(estudiantes),
                            EsTutorPadre = genEsTutorPadre(estudiantes),
                            TiempoCasaColegio = estudiantes$traveltime,
                            TiempoDeEstudio = estudiantes$studytime,
                            NumAsignaturasSuspendidas = genNumAsignaturasSuspendidas(estudiantes),
                            TieneAyudaExtraEscolar = genTieneAyudaExtraEscolar(estudiantes),
                            TieneAyudaFamiliarEscolar = genTieneAyudaFamiliarEscolar(estudiantes),
                            TieneClasesExtraDentroMateria = genTieneClasesExtraDentroMateria(estudiantes),
                            TieneActividadesExtracurricular = genTieneActividadesExtracurricular(estudiantes),
                            TieneEscuelaMaternal = genTieneEscuelaMaternal(estudiantes),  
                            QuiereEducacionSuperior = genQuiereEducacionSuperior(estudiantes),
                            TieneInternetEnCasa = genTieneInternetEnCasa(estudiantes),
                            TieneUnaRelacion = genTieneUnaRelacion(estudiantes),
                            CalidadRelacionFamiliar = estudiantes$famrel,
                            TiempoLibreDespColegio = estudiantes$freetime,
                            SaleConAmigos = estudiantes$goout,
                            ConsumoAlcoholDiario = estudiantes$Dalc,
                            ConsumoAlcoholFindes = estudiantes$Walc,
                            EstadoActualSalud = estudiantes$health,
                            NumAusenciasColegio = estudiantes$absences,
                            EsMatematica = genEsMatematica(estudiantes),
                            G1Integer = genG1Integer(estudiantes),
                            G2Integer = genG2Integer(estudiantes),
                            G3Integer = genG3Integer(estudiantes))


#########################################################
## An�lisis Exploratorio ##
# Ejecutar archivo AnalisisExploratorio.R

#########################################################

## Genero el dataset con la variable a predecir ##
DTEstudiantes_clasif <- copy(DTEstudiantes)
#Convierto las variables de notas en Aprobado/NoAprobado (variable binaria 1 o 0)
#Un estudiante est� aprobado si la suma de G1+G2+G3 es mayor o igual que 30, sin m�nimos en cada trimestre.
DTEstudiantes_clasif[, Aprobado := ifelse(G1Integer+G2Integer+G3Integer>=30,1,0)]
DTEstudiantes_clasif$G1Integer <- NULL                          
DTEstudiantes_clasif$G2Integer <- NULL   
DTEstudiantes_clasif$G3Integer <- NULL   

#La proporci�n de los estudiantes que aprueban Matem�ticas es de un 58%, que aprueban portugu�s es de 76%, y la 
#proporci�n de aprobados es de 69%
DTEstudiantes_clasif[Aprobado==1 & EsMatematica==1,.N,by=]/DTEstudiantes_clasif[EsMatematica==1,.N,]
DTEstudiantes_clasif[Aprobado==1 & EsMatematica==0,.N,by=]/DTEstudiantes_clasif[EsMatematica==0,.N,]
DTEstudiantes_clasif[Aprobado==1,.N,]/DTEstudiantes_clasif[,.N,]


#########################################################
## An�lisis Predictivo ##
# Ejecutar archivo Prediccion.R

#########################################################



