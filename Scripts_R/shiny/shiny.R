install.packages("shiny")
install.packages('rsconnect')
library(shiny)
library(rsconnect)
library(lubridate)
suppressPackageStartupMessages(suppressWarnings(library(data.table)))



rsconnect::setAccountInfo(name='mm-informe-1',
                          token='F93FFE771C4F48B293B0E4C630E34A95',
                          secret='2W7o6YN6+ow0us2hMcP1ynAflVl25NJjELFaswe0')


runExample("01_hello")
runExample("01_hello") # a histogram
runExample("02_text") # tables and data frames
runExample("03_reactivity") # a reactive expression
runExample("04_mpg") # global variables
runExample("05_sliders") # slider bars
runExample("06_tabsets") # tabbed panels este esta guay
runExample("07_widgets") # help text and submit buttons
runExample("08_html") # Shiny app built from HTML
runExample("09_upload") # file upload wizard
runExample("10_download") # file download wizard
runExample("11_timer") # an automated timer
runExample("09_upload") # file upload wizard


datos <- read.csv("C:/Users/u0698118/Desktop/R/APP/data/formalizadas.csv", 
               header=TRUE, sep=';', stringsAsFactors = F)
datos <- as.data.table(datos)

datos$DIA_ALTA <- dmy(datos$DIA_ALTA)
datos$DIA_FORM <- dmy(datos$DIA_FORM)

save(datos,file="datos.RData")

runApp("APP")


library(datasets)

aux <- datos[,.N,by=DIA_ALTA]

plot(aux$DIA_ALTA ,aux$N,type = "p", xlab = "Fecha tramitación", ylab = "Solicitudes", col="blue")

aux <- datos[!is.na(OLEADA),.N,by=TRAMOEDAD]

hist(aux$N)

table(datos$TRAMOEDAD)

pie(table(datos[!is.na(OLEADA)]$TRAMOEDAD), labels = c("[35-55]","<35",">55"), col = c("red","blue","yellow"), main = "Tramo Edad")

barplot(table(datos$TARIFA,datos$RISKACTUAL), col = c("blue", "red"), 
        legend = rownames(table(datos$TARIFA,datos$RISKACTUAL)), 
        main = "Precio vs RiskScore")

switch(datos[,.N,by=c("DIA_ALTA","OLEADA")]$OLEADA,"Oleada 1" = 1,"Oleada 2" = 2,"Oleada 3" = 3)

O1 <- datos[OLEADA==1,.N,by=DIA_ALTA]
O2 <- datos[OLEADA==2,.N,by=DIA_ALTA]
O3 <- datos[OLEADA==3,.N,by=DIA_ALTA]
oleada <- c(O1,O2,O3)
switch(oleada,
       "O1" = O1,
       "O2" = O2,
       "O3" = O3)


datos
if (datos$OLEADA==1) {
  dataset <- datos[OLEADA==1,,by=DIA_ALTA]
}
if (datos$OLEADA==2) {
  dataset <- datos[OLEADA==2,,by=DIA_ALTA]  
}
if (datos$OLEADA==3) { 
  dataset <- datos[OLEADA==3,,by=DIA_ALTA]  
}


dataset2 <- datos[DIA_ALTA>='2017-06-25' & DIA_ALTA<='2017-07-01',.N,by=DIA_ALTA]
plot(dataset2$DIA_ALTA ,dataset2$N,type = "p", xlab = "Fecha tramitacion", ylab = "Solicitudes", col="blue")



