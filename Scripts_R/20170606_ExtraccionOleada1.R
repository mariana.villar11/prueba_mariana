################################################### Extraccion Publico Objetivo Oleada 1 ##########################################################

##Cargo las librerias
suppressPackageStartupMessages(suppressWarnings(library(data.table)))

##Cargo los datos
#Este archivo es la extracción directa de la tabla "PO_20170510" creada por Riesgos con la selección del PO Impactable para el piloto
PO_Piloto <- read.csv("Tabla_PO_20170510.csv", header=TRUE, sep=';', stringsAsFactors = F)
# Este archivo son los datos de los clientes del PO (anterior)
DatosPO_Piloto <- read.csv("Query_DatosPOPiloto_oleada1.csv", header=TRUE, sep=';', stringsAsFactors = F)
#Archivo con el mapeo de codigo postales y provincias
provincias <- read.csv("provincias.csv", header=TRUE, sep=';', stringsAsFactors = F)
#Es la lista de señuelos que se han añadido en este piloto
senuelos <- read.csv("senuelos.csv", header=TRUE, sep=';', stringsAsFactors = F)
PO_Piloto <- as.data.table(PO_Piloto)
DatosPO_Piloto <- as.data.table(DatosPO_Piloto)
provincias <- as.data.table(provincias)
senuelos <- as.data.table(senuelos)

##Empiezo a trabajar en el formato de la tabla del PO
#Me quedo con las variables que necesito y lo copio para no modificar el original
DT <- copy(DatosPO_Piloto[,.(SDNI,NOMCLI,APELL1,APELL2,CALLE,PORTAL,PISO,PUERTA,BLOQUE,COMPDOM,POBLACION,CDPOSTAL,CDPROVINCIA,TLF1,TLF2,EMAIL,CLIENTECAIXA)])
#Le cambio el nombre a la provincia de Alava, ya que originalmente tiene puesto dos nombres
provincias$PROVINCIA <- ifelse(provincias$COD_PROVINCIA==1,"ALAVA",provincias$PROVINCIA)
#Incluyo la provincia en mi Data table
DT <- merge(DT, provincias, by.x = "CDPROVINCIA", by.y = "COD_PROVINCIA")
#Me quedo sólo con las variables que me interesan
DT <- DT[,.(SDNI,NOMCLI,APELL1,APELL2,CALLE,PORTAL,PISO,PUERTA,BLOQUE,COMPDOM,POBLACION,CDPOSTAL,PROVINCIA,TLF1,TLF2,EMAIL,CLIENTECAIXA)]
#Creo una variable nueva, con la condicion de que el primer teléfono sea un movil, si lo es me quedo con ese, sino miro el segundo telefono, si es un movil
#me quedo con ese, sino lo pongo a NA, esto es porque necesitamos que el teléfono que pasemos sea un móvil
DT[,Telefono:=ifelse(substring(TLF1,1,1)==6 | substring(TLF1,1,1)==7, TLF1, ifelse(substring(TLF2,1,1)==6 | substring(TLF2,1,1)==7, TLF2, NA))]
#Me quito las variables tef1 y tef2 y me quedo con la variable Telefono que he construido yo
DT <- DT[,.(SDNI,NOMCLI,APELL1,APELL2,CALLE,PORTAL,PISO,PUERTA,BLOQUE,COMPDOM,POBLACION,CDPOSTAL,PROVINCIA,Telefono,EMAIL,CLIENTECAIXA)]
#Creo una variable codigo postal agregandoles un 0 a las que empiezan por 0 (ya que no está)
DT[,CodigoPostal1:=ifelse(nchar(CDPOSTAL)==4,paste0(0,CDPOSTAL),CDPOSTAL)]
#Arreglo la dirección y les asigno vacio a los NA, ya que al exportarlo en csv sale el NA
DT$PORTAL <- ifelse(is.na(DT$PORTAL),"",DT$PORTAL)
DT$PISO <- ifelse(is.na(DT$PISO),"",DT$PISO)
DT$PUERTA <- ifelse(is.na(DT$PUERTA),"",DT$PUERTA)
DT$BLOQUE <- ifelse(is.na(DT$BLOQUE),"",DT$BLOQUE)
DT$COMPDOM <- ifelse(is.na(DT$COMPDOM),"",DT$COMPDOM)
#Arreglo las variables del domicilio para poner el formato necesario de Telefónica que son los que gestionan los envios
DT[,':='(PISO1=ifelse(PISO!="" & (PUERTA!="" | BLOQUE!="" | (COMPDOM!="" & COMPDOM!=POBLACION & COMPDOM!=PROVINCIA)), paste0(PISO,"-"), PISO),
         PUERTA1=ifelse(PUERTA!="" & (BLOQUE!="" | (COMPDOM!="" & COMPDOM!=POBLACION & COMPDOM!=PROVINCIA)), paste0(PUERTA,"-"), PUERTA),
         BLOQUE1=ifelse(BLOQUE!="" & (COMPDOM!="" & COMPDOM!=POBLACION & COMPDOM!=PROVINCIA), paste0(BLOQUE,"-"), BLOQUE),
         COMPDOM1=ifelse(COMPDOM==POBLACION | COMPDOM==PROVINCIA,"", COMPDOM))]
#Agrego una variable nueva que sea el resto del domicilio con la informacion recopilada, ya que es el formato requerido
DT[,RestoDomicilio := paste0(PISO1, PUERTA1, BLOQUE1, COMPDOM1)] 
#Me quedo con las variables que quiero
DT <- DT[,.(SDNI,NOMCLI,APELL1,APELL2,CALLE,PORTAL,RestoDomicilio,POBLACION,CDPOSTAL,PROVINCIA,Telefono,EMAIL,CLIENTECAIXA)]
#le cambio los nombres a las variables
setnames(DT, old = c("SDNI","NOMCLI","APELL1","APELL2","CALLE","PORTAL","RestoDomicilio","POBLACION","CDPOSTAL","PROVINCIA","Telefono","EMAIL","CLIENTECAIXA"), 
       new = c("DNI","Nombre","Apellido1","Apellido2","Calle","Numero","RestoDomicilio","Poblacion","CodigoPostal","Provincia","Telefono","Email","CLIENTECAIXA"))

#Compruebo que los DNI son únicos
dniUnic <- DT[,.N,by=DNI] 
#Veo que hay número de movil repetidos, y esto no puede ser, ya que la dupla DNI+movil tiene que ser unica
telefunic <- DT[,.N,by=Telefono] #hay 4702 clientes que no tienen el movil informado (solo fijo), hay que excluirlos, y los duplicados también
telefunic <- telefunic[N==1,] #me quedo con los que están solo una vez
#cruzo las tablas para quedarme solo con los que tienen telefonos unicos
DT <- merge(DT,telefunic, by.x = "Telefono", by.y = "Telefono")
DT <- DT[,.(DNI,Nombre,Apellido1,Apellido2,Calle,Numero,RestoDomicilio,Poblacion,CodigoPostal,Provincia,Telefono,Email,CLIENTECAIXA)]

#voy a poner a nulos los emails que se repiten
emailunic <- DT[,.N,by=Email] 
emailunic[,emailValido:=ifelse(N==1,1,0)]
emailunic[,EMAIL:=ifelse(emailValido==1,Email,NA)]
DT <- merge(DT,emailunic, by.x = "Email", by.y = "Email")
DT <- DT[,.(DNI,Nombre,Apellido1,Apellido2,Calle,Numero,RestoDomicilio,Poblacion,CodigoPostal,Provincia,Telefono,EMAIL,CLIENTECAIXA)]
#creo una variable que se llame email valido para excluir algunos dominios de tienda o erroneos
DT[,emailValido := ifelse(EMAIL %like% "@MPC" | EMAIL %like% "@COMPLUTEL" | EMAIL %like% "@ADMCOMUNICACIONES" | EMAIL %like% "@COMMCENTER" | 
                          EMAIL %like% "@TELYMAN" | EMAIL %like% "@MOVICELL" | EMAIL %like% "@PICTEL" | EMAIL %like% "@NOTIENE" | is.na(EMAIL),0,1)]
DT[,Email:=ifelse(emailValido==1,EMAIL,NA)]
DT <- DT[,.(DNI,Nombre,Apellido1,Apellido2,Calle,Numero,RestoDomicilio,Poblacion,CodigoPostal,Provincia,Telefono,Email,CLIENTECAIXA)]
#tengo que excluir del PO a los clientes caixa
DT <- DT[CLIENTECAIXA==0,]
#Uno las dos tablas ya que en la tabla original tengo la variable de riesgo que es por la que voy a segmentar
DT <- merge(DT,PO_Piloto, by.x = "DNI", by.y = "SDNI")
#creo la variable del tramo edad que es por la que voy a segmentar
DT[,tramoEdad:=ifelse(EDAD_ACTUAL<35,"<35",ifelse(EDAD_ACTUAL>=35 & EDAD_ACTUAL<=55,"[35-55]",">55"))]


##Creacion de los botes
#Ahora voy a empezar con la creacion de los botes por las dos variables a segmentar, riesgo y edad
DTAux <- copy(DT)
#Inicializo las variables
DTAux[,usado:=0]
DTAux[,Bote:=as.integer(NA)]
DTAux[,Producto:=as.integer(NA)]
#Para la oleada 1 sólo tendremos el precio 15.35%tin por lo tanto solo hacemos un DT con las proporciones de riesgo decididas
C_Riesgo3 <- DTAux[RISKSCORE==3,] #53%  ///los pongo todos, entonces 100% son 88149/// 
C_Riesgo4 <- DTAux[RISKSCORE==4,] #29%  ///25564
C_Riesgo5 <- DTAux[RISKSCORE==5,] #11% ///9696
C_Riesgo6 <- DTAux[RISKSCORE==6,] #7% ///6170
#este es el DT que estoy construyendo con las proporciones de riesgo decididas para luego hacer un sample de este y que los botes sigan esta distribucion
#del riesgo
C_Riesgo <- C_Riesgo3 ##los riesgos 3 los pongo todos para aprovechar y a partir de alli balanceo
#los demas rangos
sample <- sample.int(n = nrow(C_Riesgo4), size = 21118, replace = FALSE)
C_Riesgo <- rbind(C_Riesgo,C_Riesgo4[sample,])
sample <- sample.int(n = nrow(C_Riesgo5), size = 8010, replace = FALSE)
C_Riesgo <- rbind(C_Riesgo,C_Riesgo5[sample,])
sample <- sample.int(n = nrow(C_Riesgo6), size = 5097, replace = FALSE)
C_Riesgo <- rbind(C_Riesgo,C_Riesgo6[sample,])

##BOTE 2 CARTA/TMKT/SMS/SMS o EMAIL
#En telemkt hay como máximo 500 llamadas
#En el bote 2 voy a ponderar mas los mayores, 42% que son 48036, un 45% medianos y un 13% jovenes
C_bote2 <- C_Riesgo[tramoEdad==">55" & usado==0,] ## estos los meto todos
sample <- sample.int(n = nrow(C_Riesgo[tramoEdad=="[35-55]" & usado==0,]), size = 21313, replace = FALSE)
C_bote2 <- rbind(C_bote2,C_Riesgo[tramoEdad=="[35-55]" & usado==0,][sample,])
sample <- sample.int(n = nrow(C_Riesgo[tramoEdad=="<35" & usado==0,]), size = 5746, replace = FALSE)
C_bote2 <- rbind(C_bote2,C_Riesgo[tramoEdad=="<35" & usado==0,][sample,])
#son 9772 clientes de los cuales en la oleada 1 voy a meter 500, sin email
sample <- sample.int(n = nrow(C_bote2[is.na(Email),]), size = 500, replace = FALSE)
Bote2 <- C_bote2[is.na(Email),][sample,]
#de los q cumplen los criterios 2, y tienen mail hay 36083, tengo que samplear 500
sample <- sample.int(n = nrow(C_bote2[!is.na(Email),]), size = 500, replace = FALSE)
Bote2 <- rbind(Bote2,C_bote2[!is.na(Email),][sample,])
Bote2$usado <- 1 #los marco como usados
setkey(Bote2,DNI)
setkey(DTAux,DNI)
DTAux[Bote2$DNI]$usado <- 1 ##los marco los que he usado
setkey(C_Riesgo,DNI)
C_Riesgo[Bote2$DNI]$usado <- 1
Bote2$Bote <- 2
DTAux[Bote2$DNI]$Bote <- 2  ##marco al bote que pertenece
Bote2$Producto <- 2
DTAux[Bote2$DNI]$Producto <- 2  ##marco el producto 2

##BOTE 1
#13%jovenes, 42% mayores y 42% adultos, pongo todos los mayores ya q son menos q los medianos
C_bote1 <- C_Riesgo[tramoEdad==">55" & usado==0,]  ## estos los meto todos, son 23775
sample <- sample.int(n = nrow(C_Riesgo[tramoEdad=="[35-55]" & usado==0,]), size = 21313, replace = FALSE)
C_bote1 <- rbind(C_bote1,C_Riesgo[tramoEdad=="[35-55]" & usado==0,][sample,])
sample <- sample.int(n = nrow(C_Riesgo[tramoEdad=="<35" & usado==0,]), size = 5746, replace = FALSE)
C_bote1 <- rbind(C_bote1,C_Riesgo[tramoEdad=="<35" & usado==0,][sample,])
C_bote1 <- C_bote1[!is.na(Email),]
#tengo 43569 que cumplen el criterio 1 y tienen mail, de estos voy a samplear 1500
sample <- sample.int(n = nrow(C_bote1), size = 1500, replace = FALSE)
Bote1 <- C_bote1[sample,]
Bote1$usado <- 1
setkey(Bote1,DNI)
setkey(DTAux,DNI)
DTAux[Bote1$DNI]$usado <- 1 ##los marco los que he usado
setkey(C_Riesgo,DNI)
C_Riesgo[Bote1$DNI]$usado <- 1
Bote1$Bote <- 1
DTAux[Bote1$DNI]$Bote <- 1  ##marco al bote que pertenece
Bote1$Producto <- 2
DTAux[Bote1$DNI]$Producto <- 2  ##marco el producto 2

##BOTE 3
#Aleatorio, solo criterio de email valido, y riesgo pero porque es producto caro
#asi que es un 33% los tres tramos
C_bote3 <- C_Riesgo[tramoEdad=="<35" & usado==0,] 
sample <- sample.int(n = nrow(C_Riesgo[tramoEdad=="[35-55]" & usado==0,]), size = 9145, replace = FALSE)
C_bote3 <- rbind(C_bote3,C_Riesgo[tramoEdad=="[35-55]" & usado==0,][sample,])
sample <- sample.int(n = nrow(C_Riesgo[tramoEdad==">55" & usado==0,]), size = 9145, replace = FALSE)
C_bote3 <- rbind(C_bote3,C_Riesgo[tramoEdad==">55" & usado==0,][sample,])
C_bote3 <- C_bote3[!is.na(Email),] ##tengo 27058 que cumplen criterio 3 y tienen mail
#de estos voy a samplear 2500
sample <- sample.int(n = nrow(C_bote3), size = 2500, replace = FALSE)
Bote3 <- C_bote3[sample,]
Bote3$usado <- 1
setkey(Bote3,DNI)
setkey(DTAux,DNI)
DTAux[Bote3$DNI]$usado <- 1 ##los marco los que he usado
setkey(C_Riesgo,DNI)
C_Riesgo[Bote3$DNI]$usado <- 1
Bote3$Bote <- 3
DTAux[Bote3$DNI]$Bote <- 3  ##marco al bote que pertenece
Bote3$Producto <- 2
DTAux[Bote3$DNI]$Producto <- 2  ##marco el producto 2
#Me guardo los botes originales y el dt de donde he partido
save(Bote1, file = "Bote1_todo.RData")
save(Bote2, file = "Bote2_todo.RData")
save(Bote3, file = "Bote3_todo.RData")
save(DTAux, file = "20170606_DT.RData")
#∟Me quedo solo con las variables que voy a enviar en el fichero
Bote1 <- Bote1[,.(DNI,Nombre, Apellido1, Apellido2, Calle, Numero, RestoDomicilio, Poblacion,CodigoPostal, Provincia, Telefono, Email)]
Bote2 <- Bote2[,.(DNI,Nombre, Apellido1, Apellido2, Calle, Numero, RestoDomicilio, Poblacion,CodigoPostal, Provincia, Telefono, Email)]
Bote3 <- Bote3[,.(DNI,Nombre, Apellido1, Apellido2, Calle, Numero, RestoDomicilio, Poblacion,CodigoPostal, Provincia, Telefono, Email)]
#agrego la variable producto, producto 1 es el 9,9, producto 2 es el 15,3
Bote1 <- Bote1[,ModeloCarta:=rep(2, len = nrow(Bote1))]
Bote2 <- Bote2[,ModeloCarta:=rep(2, len = nrow(Bote2))]
Bote3 <- Bote3[,ModeloCarta:=rep(2, len = nrow(Bote3))]
Bote1 <- Bote1[,':='(Contacto1="Carta",Contacto2="Email",Contacto3="SMS",Contacto4="Email")]
Bote2 <- Bote2[,':='(Contacto1="Carta",Contacto2="SMS",Contacto3="Tmkt",Contacto4="SMS/Email")]
Bote3 <- Bote3[,':='(Contacto1="Email",Contacto2="SMS",Contacto3="Email",Contacto4="SMS")]
#agrego los señuelos a cada bote
senuelos <- senuelos[,.(NIF,NOMCLI,APELL1,APELL2,DIRECCION,PORTAL,PISO,PUERTA,COMPDOM,CP,POBLACION,PROVINCIA,MOVIL,EMAIL)]
senuelos[,RestoDomicilio := paste0(PISO," ", PUERTA, " ","Bloque ", " ", COMPDOM)] 
senuelos <- senuelos[,.(NIF,NOMCLI,APELL1,APELL2,DIRECCION,PORTAL,RestoDomicilio,POBLACION,CP,PROVINCIA,MOVIL,EMAIL)]
setnames(senuelos, old = c("NIF","NOMCLI","APELL1","APELL2","DIRECCION","PORTAL","RestoDomicilio", "POBLACION", "CP","PROVINCIA","MOVIL","EMAIL"), 
                 new = c("DNI","Nombre","Apellido1","Apellido2","Calle","Numero","RestoDomicilio","Poblacion","CodigoPostal","Provincia","Telefono","Email"))
senuelos <- senuelos[,ModeloCarta:=rep(2, len = nrow(senuelos))]
bote1Senuelos <- copy(senuelos)
bote2Senuelos <- copy(senuelos)
bote3Senuelos <- copy(senuelos)
bote1Senuelos <- bote1Senuelos[,':='(Contacto1="Carta",Contacto2="Email",Contacto3="SMS",Contacto4="Email")]
bote2Senuelos <- bote2Senuelos[,':='(Contacto1="Carta",Contacto2="SMS",Contacto3="Tmkt",Contacto4="SMS/Email")]
bote3Senuelos <- bote3Senuelos[,':='(Contacto1="Email",Contacto2="SMS",Contacto3="Email",Contacto4="SMS")]
#quito a quien corresponda de cada bote
bote1Senuelos <- bote1Senuelos[!DNI=="50381690Y",]
bote2Senuelos <- bote2Senuelos[!DNI=="50381690Y",]
#agrego los señuelos a los botes
Bote1 <- rbind(Bote1,bote1Senuelos)
Bote2 <- rbind(Bote2,bote2Senuelos)
Bote3 <- rbind(Bote3,bote3Senuelos)
#Escribir los ficheros, en el formato a enviar a telefonica
write.table(Bote1, 'Bote1_.txt', sep='\t', row.names = FALSE, quote = FALSE)
write.table(Bote2, 'Bote2_.txt', sep='\t', row.names = FALSE, quote = FALSE)
write.table(Bote3, 'Bote3_.txt', sep='\t', row.names = FALSE, quote = FALSE)



















