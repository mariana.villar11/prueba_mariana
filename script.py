# -*- coding: utf-8 -*-
"""
Created on Fri Apr 13 08:52:22 2018

@author: mariana.villar uru
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error, r2_score
import statsmodels.api as sm

from sklearn.datasets import make_classification
from IPython.core.interactiveshell import InteractiveShell
InteractiveShell.ast_node_interactivity = "all"

import pymrmr
import mifs


data_csv = pd.read_csv('C:/Users/mariana.villar/Desktop/Python/ejemplo_fuga/prueba_mariana/Rimac_fuga_pm_train.txt', sep='\t')
data_csv.head()

aux = pd.dataframe(data_csv.describe())


data_csv.mean()
# variables
# TARGET,ANT_PM,EDAD,PROM_SALDO_ACTIVO_TOTAL_ULT_3M,PROM_NUM_PCON_ULT_6M
#,PROM_CUOTA_PCON_ULT_6M,PROM_IMP_NOMINA_ULT_6M,DELTA_IMP_NOMINA_0_5
#,PROM_TEN_PASIVO_ULT_6M,DELTA_SALDO_PASIVO_TOTAL_0_5,
#,PROM_TARJETAS_AJENAS_ULT_6M, PROM_NUMERO_ENTIDADES_ULT_6M,  PROM_SCORE_RIESGO_ULT_3M
#,DELTA_LIMITE_LINEA_TDC_0_5,  PROM_LIMITE_LINEA_TDC_ULT_1M
#,PROM_LINEA_DISPONIBLE_TDC_ULT_3M,  PROM_SALDO_ACTUAL_TDC_ULT_1M
#,PROM_SALDO_CUOTAS_TDC_ULT_6M,  PROM_IMPORTE_TDC_ULT_3M
#,PROM_NUM_OPERS_TDC_ULT_6M

data_csv.groupby(['EDAD','TARGET'])

auxiliar = pd.DataFrame(data_csv.corr(method='pearson',min_periods=1))

aux = data_csv[['TARGET','EDAD']]

conteo = data_csv.groupby(['TARGET']).mean()

aux2 = aux.groupby(['TARGET']).count()

dt1 = data_csv[data_csv['TARGET'] == 1]
dt0 = data_csv[data_csv['TARGET'] == 0]

edad = data_csv[['TARGET','EDAD']].groupby(['TARGET']).sum()

edad2 = data_csv[['TARGET','EDAD']].groupby(['EDAD'], axis=0).sum()

plt.plot(edad2)
plt.show()

edad = data_csv[['TARGET','EDAD']].groupby(['EDAD'], axis=0).count()
plt.plot(edad)
plt.show()

edad1 = dt1[['TARGET','EDAD']].groupby(['EDAD'], axis=0).count()
plt.plot(edad1)
plt.show()

edad0 = dt0[['TARGET','EDAD']].groupby(['EDAD'], axis=0).count()
plt.plot(edad0)
plt.show()

prop = (8613.0/17214.0)*100  ##50% de 1's


target = pd.DataFrame(data_csv, columns=['TARGET'])

data = pd.DataFrame(data_csv, columns=['ANT_PM','EDAD','PROM_SALDO_ACTIVO_TOTAL_ULT_3M','PROM_NUM_PCON_ULT_6M','PROM_CUOTA_PCON_ULT_6M','PROM_IMP_NOMINA_ULT_6M'
                                       ,'DELTA_IMP_NOMINA_0_5','PROM_TEN_PASIVO_ULT_6M','DELTA_SALDO_PASIVO_TOTAL_0_5','PROM_TARJETAS_AJENAS_ULT_6M' 
                                       ,'PROM_NUMERO_ENTIDADES_ULT_6M','PROM_SCORE_RIESGO_ULT_3M','DELTA_LIMITE_LINEA_TDC_0_5','PROM_LIMITE_LINEA_TDC_ULT_1M'
                                       ,'PROM_LINEA_DISPONIBLE_TDC_ULT_3M','PROM_SALDO_ACTUAL_TDC_ULT_1M','PROM_SALDO_CUOTAS_TDC_ULT_6M','PROM_IMPORTE_TDC_ULT_3M'
                                       ,'PROM_NUM_OPERS_TDC_ULT_6M'])

model = sm.OLS(target, data).fit()
predictions = model.predict(data)

model.summary()

Y_train = data_csv['TARGET']
X_train = data_csv.drop('TARGET', axis=1)



pymrmr.mRMR(df, 'MIQ',6)
