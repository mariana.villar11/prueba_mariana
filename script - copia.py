# -*- coding: utf-8 -*-
"""
Created on Fri Apr 13 08:52:22 2018

@author: mariana.villar uru
"""


import numpy as np
import pandas as pd

data_csv = pd.read_csv('C:/Users/mariana.villar/Desktop/Python/ejemplo_fuga/prueba_mariana/Rimac_fuga_pm_train.txt', sep='\t')
data_csv.head()

data_csv.describe()

df = data_csv
df

df[(df.col1 == 1) & (df.col2 == 1)]

data_csv.mean()
# variables
# TARGET,ANT_PM,EDAD,PROM_SALDO_ACTIVO_TOTAL_ULT_3M,PROM_NUM_PCON_ULT_6M
#,PROM_CUOTA_PCON_ULT_6M,PROM_IMP_NOMINA_ULT_6M,DELTA_IMP_NOMINA_0_5
#,PROM_TEN_PASIVO_ULT_6M,DELTA_SALDO_PASIVO_TOTAL_0_5,
#,PROM_TARJETAS_AJENAS_ULT_6M, PROM_NUMERO_ENTIDADES_ULT_6M,  PROM_SCORE_RIESGO_ULT_3M
#,DELTA_LIMITE_LINEA_TDC_0_5,  PROM_LIMITE_LINEA_TDC_ULT_1M
#,PROM_LINEA_DISPONIBLE_TDC_ULT_3M,  PROM_SALDO_ACTUAL_TDC_ULT_1M
#,PROM_SALDO_CUOTAS_TDC_ULT_6M,  PROM_IMPORTE_TDC_ULT_3M
#,PROM_NUM_OPERS_TDC_ULT_6M

g = df.groupby(['EDAD','ANT_PM'])
g[['EDAD','ANT_PM']].mean()

df2 = pd.DataFrame({'v1': [1,2,3],
                    'v2': [2,3,4],
                    'by1': ["red", "blue", 1],
                    'by2': ["wet","dry", 99]})



