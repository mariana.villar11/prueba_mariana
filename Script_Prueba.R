
suppressPackageStartupMessages(suppressWarnings(library(data.table)))

empleados <- read.csv("empleados.csv", header=TRUE, sep=';', stringsAsFactors = F)
relaciones <- read.csv("relaciones_contractuales.csv", header=TRUE, sep=';', stringsAsFactors = F)

empleados <- as.data.table(empleados)
relaciones <- as.data.table(relaciones)

dgt <- empleados[,.N,by=DGT]
dt <- empleados[,.N,by=DT]
od <- empleados[,.N,by=OD]


empleados <- empleados[,id:=paste(DGT,DT,OD,CLAVE, sep='',collapse = NULL)]
relaciones <- relaciones[,id:=paste(ID_DGT,ID_DT,ID_OD,CLAVE, sep='',collapse = NULL)]

empleados2 <- empleados[,id2:=paste(DGT,DT,OD,CLAVE,NIF, sep='',collapse = NULL)]
relaciones2 <- relaciones[,id2:=paste(ID_DGT,ID_DT,ID_OD,CLAVE,NIF, sep='',collapse = NULL)]

DT <- merge(empleados, relaciones, all.x = TRUE, by.x = "id", by.y = "id")
DT2 <- merge(empleados2, relaciones2, all.x = TRUE, by.x = "id2", by.y = "id2")

names(DT)
DT_fil <- DT[,.(id,DGT.y,DT.y,OD.y,TIPO_OD,OC,SMA.y,CLAVE.y,REL_CONTR,NIF.x,NIF.y,PERFIL,NUUMA.x,NUUMA.y,
            CL,ID_CAT,CATALOGACION,ID_COL,COLECTIVO,ID_TIPO_MEDIADOR)]


aux <- DT[,.N,by=id]


names(empleados)
names(relaciones)
nif_empl <- empleados[,(NIF)]
nif_empl <- unique(nif_empl)

names(DT_fil)
DT_fil <- DT_fil[CATALOGACION!='CORREDOR DE SEGUROS' & CATALOGACION!='OPERADOR BANCA SEGUROS VINCULADO' 
                 & CATALOGACION!='OPERADOR BANCA SEGUROS EXCLUSIVO' &
                 CL!='CORREDORES-MANTENIMIENTO CARTERA' & CL!='AGENTE VINCULADO-ACUERDOS DISTRIBUCION',]

aux <- DT_fil[,.N,by=NIF.x]

catalogo <- DT_fil[,.N,by=c("CATALOGACION","ID_COL","COLECTIVO","CL")]

colectivo <- DT_fil[,.N,by=c("CATALOGACION","ID_COL","COLECTIVO")]


DT_nif <- DT_fil[,.(DGT.y,DT.y,OD.y,TIPO_OD,CATALOGACION,ID_COL,COLECTIVO,CL,NIF.x,NIF.y)]
DT_nif <- unique(DT_nif)

catalogo <- DT_nif[,.N,by=c("CATALOGACION","ID_COL","COLECTIVO","CL")]
colectivo <- DT_nif[,.N,by=c("CATALOGACION","ID_COL","COLECTIVO")]


DT_nif[,agrupacion:=ifelse((CATALOGACION=='AGENTE DE SEGUROS EXCLUSIVO' & ID_COL!='DELEGADO') |
                             CATALOGACION=='AGENTES DE SEGUROS VINCULADO' | 
                             CATALOGACION=='AGENTE LIBRE PRESTACIÓN DE SERVICIOS','AGENTE',
                      ifelse(ID_COL=='DELEGADO' | (ID_COL=='' & CL=='Delegados con oficina MAPFRE'),'DELEGADO',
                        'OTROS'))]


DT_nif <- DT_nif[,.(DGT.y,DT.y,OD.y,TIPO_OD,CATALOGACION,ID_COL,COLECTIVO,agrupacion,NIF.x,NIF.y)]
DT_nif <- unique(DT_nif)


DT_nif[,num_emp:=.N,by=c("NIF.y","agrupacion","DGT.y","DT.y","OD.y")]

DT_nif[NIF.y=='050849035Z',]
cuenta_empl <- DT_nif[agrupacion=='DELEGADO',sum(num_emp),by=c("DGT.y","DT.y","OD.y")]

aux<-DT_nif[agrupacion=='DELEGADO' & OD.y=='MOSTOLES',]



DT_deleg <- copy(DT_nif[,.(DGT.y,DT.y,OD.y,TIPO_OD,CATALOGACION,ID_COL,COLECTIVO,agrupacion,NIF.y)])
DT_deleg <- unique(DT_deleg)
DT_deleg[,num_delegados:=.N,by=c("agrupacion","DGT.y","DT.y","OD.y")]


DT_emp_del <- merge(DT_nif, DT_deleg, all.x = TRUE, by.x = c("NIF.y","agrupacion","DGT.y","DT.y","OD.y"),
                    by.y = c("NIF.y","agrupacion","DGT.y","DT.y","OD.y"))

names(DT_emp_del)
DT_emp_del <- DT_emp_del[,.(DGT.y,DT.y,OD.y,TIPO_OD.x,CATALOGACION.x,ID_COL.x,COLECTIVO.x,agrupacion,NIF.x,NIF.y,
                            num_emp,num_delegados)]


dt_delegados <- copy(DT_emp_del[agrupacion=='DELEGADO',.(DGT.y,DT.y,OD.y,TIPO_OD.x,num_delegados)])
dt_delegados <- unique(dt_delegados)                    
dt_delegados[,sum(num_delegados)]
N_delegados <- dt_delegados[,sum(num_delegados),by=c("DGT.y","DT.y","OD.y")]


dt_empleados <- copy(DT_emp_del[agrupacion=='DELEGADO',.(DGT.y,DT.y,OD.y,TIPO_OD.x,num_emp)])
dt_empleados[,sum(num_emp)]
N_empleados <- dt_empleados[,sum(num_emp),by=c("DGT.y","DT.y","OD.y")]

write.table(N_delegados, 'N_delegados.txt', sep='\t', row.names = FALSE, quote = FALSE)
write.table(N_empleados, 'N_empleados.txt', sep='\t', row.names = FALSE, quote = FALSE)




